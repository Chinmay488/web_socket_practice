from django.db import models
from channels.layers import get_channel_layer
import json
from asgiref.sync import async_to_sync

class Chat(models.Model):
    
    message = models.TextField(max_length=100)
    
    
    
        
    def save(self,*args,**kwargs):
        channel_layer = get_channel_layer()
        room_group_name = "my_group_name"

        super(Chat,self).save(*args,**kwargs)
        
        list_of_messages = list()
        chat_data = Chat.objects.all()
        for chat_obj in chat_data:
            list_of_messages.append(chat_obj.message)
            
        async_to_sync(channel_layer.group_send)(
            room_group_name,{
                'value': list_of_messages,
                'type':'notify'
            }
        )
        
        
        
        
        

        
