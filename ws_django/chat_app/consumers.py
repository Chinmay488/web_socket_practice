from email import message
from channels.generic.websocket import AsyncWebsocketConsumer
import json
from chat_app.models import Chat
from asgiref.sync import async_to_sync,sync_to_async


class ChatConsumer(AsyncWebsocketConsumer):

    def get_messages(self):
        messages = Chat.objects.all()
        list_of_messages = list()
        for msg in messages:
            list_of_messages.append(msg.message)
        
        return list_of_messages

    async def connect(self):
        self.room_name = "my_room_name"
        self.room_group_name = "my_group_name"
        
        await(self.channel_layer.group_add)(
            self.room_group_name,self.channel_name
        )
        await self.accept()
        list_of_messages = await sync_to_async(self.get_messages)()
        data = {
            "status":"Connected",
            "data":list_of_messages
        }
        await self.send(text_data=json.dumps(data))
      
      
  
    def save_message(self,message):
        chat = Chat(message=message)
        chat.save()
  
    
    async def receive(self,text_data):
        received_data = json.loads(text_data)
        await sync_to_async(self.save_message,thread_sensitive=True)(received_data["data"])
       
    async def disconnect(self, code):
        print("Disconnected")
    
    
    async def notify(self,event):
        data_to_send = {
            "status":"",
            "data":event['value'],
        }
        await self.send(text_data=json.dumps(data_to_send))
    