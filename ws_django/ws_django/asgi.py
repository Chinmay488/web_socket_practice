"""
ASGI config for ws_django project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from chat_app.consumers import ChatConsumer

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ws_django.settings')

django_asgi_app = get_asgi_application()

url_patters = [
    path("chat",ChatConsumer.as_asgi(),name="Chat Consumer"),
]

application = ProtocolTypeRouter({
        # "http": django_asgi_app,
        "websocket": URLRouter(url_patters) 
    })

