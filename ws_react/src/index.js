import React from 'react';
import ReactDOM from 'react-dom/client';
import "./index.css";
import App from "./App";


const socketURL = "ws://192.168.1.146:8000/chat";
  const socket = new WebSocket(socketURL);

  socket.onopen = (event) => {
    // console.log(event);
  };

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
 <App socket={socket} />
  
);
