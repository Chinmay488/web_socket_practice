import React, { useState } from "react";

const App = (props) => {
  const [userInput, setUserInput] = useState("");
  const [chatData, setChatData] = useState([]);


  props.socket.onmessage = (event) => {
    const info = JSON.parse(event.data);
    setChatData([...info.data])
  };

  props.socket.onclose = (event) => {
    console.log("Connection Closed");
  };

  const onSendData = (event) => {
    event.preventDefault();
    const data = {
      status: "",
      data: userInput,
    };
    props.socket.send(JSON.stringify(data));
  };

  return (
    <>
      <h1>Hehehe</h1>

      <form onSubmit={onSendData} className="myForm">
        <input
          type="text"
          name="userInput"
          required={true}
          onChange={(event) => {
            const { value } = event.target;
            setUserInput(value);
          }}
          id="dataToSend"
        />
        <button type="submit">Send</button>
      </form>

        <ul>
            {
                chatData.length > 0 ? <>
                  {chatData.map((item,index)=>{
                    return <li key={`chat-${index}`}>{item}</li>
                })}
                </> : <h1>
                  Nothing Here
                </h1> 
            }
        </ul>

    </>
  );
};

export default App;
