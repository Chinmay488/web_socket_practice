
const socketURL = "ws://192.168.1.146:8000/chat";
const socket = new WebSocket(socketURL);

socket.onopen = (event)=>{
    console.log(event);
}

socket.onmessage = (event)=>{
    const list = document.getElementById("myList");
    list.innerHTML = "";
    const info = JSON.parse(event.data)
    // console.log(info.data);
    info.data.forEach((element) => {
        const item = `<li>${element}</li>`
        list.innerHTML += item;
    });
}

socket.onclose = (event)=>{
    console.log("Connection Closed")
}


// const onSendData = (event) => {
//     const userInput = document.getElementById("dataToSend").value;
//     console.log(userInput);
//     //alert("HEHEH");
//     const data = {
//       data: userInput,
//     };
//     console.log(data);
//     socket.send(JSON.stringify(data));
//     event.preventDefault();
//   };


$("#myForm").submit(function (event) {
    event.preventDefault();
    const userInput = document.getElementById("dataToSend").value;
    console.log(userInput);
    const data = {
      data: userInput,
    };
    console.log(data);
    socket.send(JSON.stringify(data));
  });