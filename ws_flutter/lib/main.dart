import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'WS Chat Flutter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String userMessage = "";

  final channel = WebSocketChannel.connect(
    Uri.parse('ws://192.168.1.146:8000/chat'),
  );

  void sendMessage() {
    dynamic info = {"status": "", "data": userMessage};

    dynamic dataToSend = jsonEncode(info);
    channel.sink.add(dataToSend);
  }

  @override
  void dispose() {
    channel.sink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder(
                // initialData: ListView.builder(),
                stream: channel.stream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    dynamic temp = snapshot.data;
                    dynamic chatMessages = jsonDecode(temp);
                    return chatMessages["data"].length > 0
                        ? ListView.builder(
                            itemCount: chatMessages["data"].length,
                            itemBuilder: (context, index) {
                              return Text(chatMessages["data"][index]);
                            },
                          )
                        : const Text("Nothing Here");
                  }
                  return Container();
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 0,
                horizontal: 10.0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      onChanged: (value) {
                        setState(() {
                          userMessage = value;
                        });
                      },
                      style: const TextStyle(fontSize: 20.0),
                      decoration: const InputDecoration(
                        hintText: "Type Here",
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: userMessage != "" ? sendMessage : null,
                    icon: const Icon(Icons.send),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
